/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.36-MariaDB : Database - his
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`his` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `his`;

/*Table structure for table `tbl_account` */

DROP TABLE IF EXISTS `tbl_account`;

CREATE TABLE `tbl_account` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` char(76) NOT NULL,
  `question` varchar(255) NOT NULL,
  `secret_answer` char(76) NOT NULL,
  `created` date NOT NULL,
  `statusid` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `statusid` (`statusid`),
  CONSTRAINT `tbl_account_ibfk_1` FOREIGN KEY (`statusid`) REFERENCES `tbl_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_account` */

insert  into `tbl_account`(`id`,`type`,`fullname`,`gender`,`username`,`password`,`question`,`secret_answer`,`created`,`statusid`) values (1,'Admin','yaw','Male','admin','$2a$12$7rMFqfwlAek4LEtynTxGuejMrALPbIDnLoDf1kIVdPHWmZR3j.kD6','What is your contact number?','$2a$12$8OTZlay8vy3oaQgW4MSUMuVoLjOztB35xW25eNS56jk53lV5maTku','2020-12-22',1);

/*Table structure for table `tbl_audit` */

DROP TABLE IF EXISTS `tbl_audit`;

CREATE TABLE `tbl_audit` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `accountid` int(1) NOT NULL,
  `username` varchar(50) NOT NULL,
  `logdate` date NOT NULL,
  `timein` varchar(50) NOT NULL,
  `timeout` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accountid` (`accountid`),
  CONSTRAINT `tbl_audit_ibfk_1` FOREIGN KEY (`accountid`) REFERENCES `tbl_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_audit` */

insert  into `tbl_audit`(`id`,`accountid`,`username`,`logdate`,`timein`,`timeout`) values (1,1,'admin','2020-12-23','09:07:36 PM','11:11:43 AM');

/*Table structure for table `tbl_patient` */

DROP TABLE IF EXISTS `tbl_patient`;

CREATE TABLE `tbl_patient` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `middleinitial` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `age` int(11) NOT NULL,
  `phone_number` varchar(11) NOT NULL,
  `blood_type` varchar(50) NOT NULL,
  `heights` varchar(50) NOT NULL,
  `weights` varchar(50) NOT NULL,
  `temperature` varchar(50) NOT NULL,
  `blood_pressure` varchar(50) NOT NULL,
  `pulse_rate` varchar(50) NOT NULL,
  `maritial_status` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_patient` */

insert  into `tbl_patient`(`id`,`firstname`,`lastname`,`middleinitial`,`date_of_birth`,`age`,`phone_number`,`blood_type`,`heights`,`weights`,`temperature`,`blood_pressure`,`pulse_rate`,`maritial_status`,`gender`,`address`,`created_at`) values (1,'ASD','ASD','A','2021-02-24',21,'123','A','33','3','3','3','3','Single','Male','asd','2021-02-24 21:32:26'),(2,'FASD','ASD','A','2021-02-27',23,'12345887878','B+','5\'4','45','35','102','34','Single','Female','bulacan','2021-02-26 09:11:28');

/*Table structure for table `tbl_physician` */

DROP TABLE IF EXISTS `tbl_physician`;

CREATE TABLE `tbl_physician` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `doctor_name` varchar(50) NOT NULL,
  `phone_number` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `department` varchar(100) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_physician` */

/*Table structure for table `tbl_room` */

DROP TABLE IF EXISTS `tbl_room`;

CREATE TABLE `tbl_room` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `room_number` varchar(50) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_room` */

/*Table structure for table `tbl_service` */

DROP TABLE IF EXISTS `tbl_service`;

CREATE TABLE `tbl_service` (
  `service_code` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `record_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`service_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9782 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_service` */

/*Table structure for table `getallpatients` */

DROP TABLE IF EXISTS `getallpatients`;

/*!50001 DROP VIEW IF EXISTS `getallpatients` */;
/*!50001 DROP TABLE IF EXISTS `getallpatients` */;

/*!50001 CREATE TABLE  `getallpatients`(
 `ID` int(1) ,
 `FIRSTNAME` varchar(100) ,
 `LASTNAME` varchar(100) ,
 `MIDDLE_INITIAL` varchar(50) ,
 `DATE_OF_BIRTH` date ,
 `AGE` int(11) ,
 `CONTACT` varchar(11) ,
 `BLOOD_TYPE` varchar(50) ,
 `HEIGHTS` varchar(50) ,
 `WEIGHTS` varchar(50) ,
 `TEMPERATURE` varchar(50) ,
 `BLOOD_PRESSURE` varchar(50) ,
 `PULSE_RATE` varchar(50) ,
 `MARITIAL_STATUS` varchar(50) ,
 `GENDER` varchar(6) ,
 `ADDRESS` text ,
 `CREATED_AT` timestamp 
)*/;

/*Table structure for table `getallphysician` */

DROP TABLE IF EXISTS `getallphysician`;

/*!50001 DROP VIEW IF EXISTS `getallphysician` */;
/*!50001 DROP TABLE IF EXISTS `getallphysician` */;

/*!50001 CREATE TABLE  `getallphysician`(
 `ID` int(1) ,
 `DOCTORNAME` varchar(50) ,
 `PHONE_NUMBER` varchar(11) ,
 `EMAIL` varchar(50) ,
 `DEPARTMENT` varchar(100) ,
 `ADDRESS` text 
)*/;

/*View structure for view getallpatients */

/*!50001 DROP TABLE IF EXISTS `getallpatients` */;
/*!50001 DROP VIEW IF EXISTS `getallpatients` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getallpatients` AS select `p`.`id` AS `ID`,`p`.`firstname` AS `FIRSTNAME`,`p`.`lastname` AS `LASTNAME`,`p`.`middleinitial` AS `MIDDLE_INITIAL`,`p`.`date_of_birth` AS `DATE_OF_BIRTH`,`p`.`age` AS `AGE`,`p`.`phone_number` AS `CONTACT`,`p`.`blood_type` AS `BLOOD_TYPE`,`p`.`heights` AS `HEIGHTS`,`p`.`weights` AS `WEIGHTS`,`p`.`temperature` AS `TEMPERATURE`,`p`.`blood_pressure` AS `BLOOD_PRESSURE`,`p`.`pulse_rate` AS `PULSE_RATE`,`p`.`maritial_status` AS `MARITIAL_STATUS`,`p`.`gender` AS `GENDER`,`p`.`address` AS `ADDRESS`,`p`.`created_at` AS `CREATED_AT` from `tbl_patient` `p` order by `p`.`id` */;

/*View structure for view getallphysician */

/*!50001 DROP TABLE IF EXISTS `getallphysician` */;
/*!50001 DROP VIEW IF EXISTS `getallphysician` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getallphysician` AS select `tbl_physician`.`id` AS `ID`,`tbl_physician`.`doctor_name` AS `DOCTORNAME`,`tbl_physician`.`phone_number` AS `PHONE_NUMBER`,`tbl_physician`.`email` AS `EMAIL`,`tbl_physician`.`department` AS `DEPARTMENT`,`tbl_physician`.`address` AS `ADDRESS` from `tbl_physician` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
